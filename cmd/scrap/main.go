package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	. "lospecscrap"
	"net/http"
	"os"
	"time"
)

func getURL(page int) string {
	return fmt.Sprintf("https://lospec.com/palette-list/"+
		"load?colorNumberFilterType=any&colorNumber=&page=%d&tag=&sortingType=default", page)
}

func main() {
	log.SetOutput(os.Stderr)
	c := http.Client{}

	var out []Palettes

	page := 1
	for {
		time.Sleep(3 * time.Second) // be nice
		log.Println("Page ", page)
		resp, err := c.Get(getURL(page))
		if err != nil {
			log.Fatalf("Error: %s", err.Error())
		}

		rawJSON, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatalf("Error: %s", err.Error())
		}

		apiResponse := &APIResponse{}
		err = json.Unmarshal(rawJSON, apiResponse)
		if err != nil {
			log.Println(err.Error())
			log.Println(string(rawJSON))
			break
		}
		if len(apiResponse.Palettes) == 0 {
			break
		}

		page++
		resp.Body.Close()
		out = append(out, apiResponse.Palettes...)
	}
	enc := json.NewEncoder(os.Stdout)
	enc.SetIndent("", "\t")
	enc.Encode(out)
}
