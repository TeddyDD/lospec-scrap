package main

import (
	"bufio"
	"encoding/json"
	"log"
	lospec "lospecscrap"
	"os"

	"github.com/ARolek/ase"
	"github.com/gosimple/slug"
	"github.com/lucasb-eyer/go-colorful"
)

func main() {
	pals := &[]lospec.Palettes{}

	buff := bufio.NewReader(os.Stdin)

	dec := json.NewDecoder(buff)
	err := dec.Decode(pals)
	if err != nil {
		log.Fatalln(err.Error())
	}

	res := ase.ASE{}
	slug.MaxLength = 16

	for i, pal := range *pals {
		log.Printf("Exporting %d pallete - %s", i, pal.Title)
		// group := ase.Group{}
		// group.Name = slug.Make(pal.Title)
		// log.Printf("Group name %s", group.Name)
		for _, colorHex := range pal.Colors {
			c, err := colorful.Hex("#" + colorHex)
			if err != nil {
				log.Fatalln(err.Error())
			}
			log.Printf("Adding color #%s", colorHex)
			aseColor := ase.Color{
				Name:  colorHex,
				Model: "RGB",
				Values: []float32{
					float32(c.R),
					float32(c.G),
					float32(c.B),
				},
				Type: "Global",
			}
			// group.Colors = append(group.Colors, aseColor)
			res.Colors = append(res.Colors, aseColor)
		}
		// res.Groups = append(res.Groups, group)
	}
	log.Println("Saving")
	f, err := os.OpenFile("lospec.ase", os.O_WRONLY|os.O_CREATE, 0655)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer f.Close()
	ase.Encode(res, f)
}
