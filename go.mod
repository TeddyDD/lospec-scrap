module lospecscrap

go 1.14

require (
	github.com/ARolek/ase v0.0.0-20150817230131-881046303523
	github.com/gosimple/slug v1.9.0
	github.com/lucasb-eyer/go-colorful v1.0.3
)
