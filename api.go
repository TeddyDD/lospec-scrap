package lospec

type APIResponse struct {
	Palettes   []Palettes `json:"palettes"`
	TotalCount float64    `json:"totalCount"`
	Shopad     string     `json:"shopad"`
}
type User struct {
	Name string `json:"name"`
	Slug string `json:"slug"`
}
type Examples struct {
	ID              string  `json:"_id"`
	Likes           float64 `json:"likes"`
	Comments        float64 `json:"comments"`
	Approval        string  `json:"approval"`
	Image           string  `json:"image"`
	Description     string  `json:"description"`
	Title           string  `json:"title"`
	Slug            string  `json:"slug"`
	User            User    `json:"user"`
	Parent          string  `json:"parent"`
	V               float64 `json:"__v"`
	ThumbnailOffset float64 `json:"thumbnailOffset"`
	DescriptionText string  `json:"descriptionText"`
}
type Likebutton struct {
	Content string  `json:"content"`
	Type    string  `json:"type"`
	Count   float64 `json:"count"`
	Liked   float64 `json:"liked"`
}
type Palettes struct {
	ID             string     `json:"_id"`
	Tags           []string   `json:"tags"`
	Colors         []string   `json:"colors"`
	Downloads      string     `json:"downloads"`
	Featured       bool       `json:"featured"`
	Likes          float64    `json:"likes"`
	Comments       float64    `json:"comments"`
	Title          string     `json:"title"`
	Hashtag        string     `json:"hashtag"`
	Description    string     `json:"description"`
	Slug           string     `json:"slug"`
	User           User       `json:"user,omitempty"`
	NumberOfColors float64    `json:"numberOfColors"`
	IsNew          bool       `json:"isNew"`
	Examples       []Examples `json:"examples"`
	ColorsArray    []string   `json:"colorsArray"`
	TotalPalettes  float64    `json:"totalPalettes"`
	MinWidth       float64    `json:"minWidth"`
	Height         float64    `json:"height"`
	Likebutton     Likebutton `json:"likebutton"`
	ThumbnailWidth float64    `json:"thumbnailWidth"`
}
